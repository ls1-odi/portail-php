<div class="semainier">        


<table class="table">

<tr class="entete">
<th>  </th>
<th> Cours </th>
<th> TD </th>
<th> TP </th>
<th> Remarque </th>
</tr>



<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 08/01 au 13/01</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td > 
<!-- TP -->

</td>
<td class="remarque"> 
<!-- REMARQUE -->
</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->



<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 15/01 au 20/01</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td> 
<!-- TP -->

</td>
<td class="remarque"> 
<!-- REMARQUE -->
</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->


<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 22/01 au 27/01</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td> 
<!-- TP -->

</td>
<td class="remarque"> 
<!-- REMARQUE -->

</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->



<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 29/01 au 03/02</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td>
<!-- TP -->

</td>
<td class="remarque"> 
<!-- REMARQUE -->
</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->


<!-- ========================================================= -->
<tr class="seance" >
<!-- FIN SEANCE -->
<td>du 05/02 au 10/02</td>
<td>
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td>
<!-- TP -->
</td>
<td class="remarque"> 
<!-- REMARQUE -->
</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->


<!-- ========================================================= -->
<tr class="seance" >
<td>du 12/02 au 17/02</td>
<td>
</td>
<td> 
</td>
<td>
<!-- TP -->

</td>
<td class="remarque">
<!-- REMARQUE -->

</td>
</tr>
<!-- ========================================================= -->



<!-- ========================================================= -->
<!-- FIN SEANCE -->
<tr class="seance" >
<td>du 19/02 au 24/02</td>
<td>
<!-- COURS -->

</td>
<td>
<!-- TD -->

</td>
<td>
<!-- TP -->

</td>
<td class="remarque"> 

</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->





<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="libre" >
<td>du 26/02 au 02/03</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td> 
<!-- TP -->

</td>
<td class="remarque"> 
<!-- REMARQUE -->
interruption pédagogique hiver
</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->

<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 04/03 au 09/03</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td>
<!-- TP -->

</td>
<td class="remarque"> 
<!-- REMARQUE -->

</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->


<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 11/03 au 16/03</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td> 
<!-- TP -->

</td>
<td class="remarque"> 
<!-- REMARQUE -->

</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->

<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 18/03 au 23/03</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td> 
<!-- TP -->

</td>
<td class="remarque"> 
<!-- REMARQUE -->
</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->



<!-- ========================================================= -->
<tr class="seance" >
<td>du 25/03 au 30/03</td>
<td> <!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td> 
<!-- TP -->
</td>
<td class="remarque"> 
<!-- REMARQUE -->
</td>
</tr>
<!-- ========================================================= -->


<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 01/04 au 06/04</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td> 
<!-- TP -->

</td>
<td class="remarque"> 
<!-- REMARQUE -->
lundi 1er avril férié
</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->


<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 8/04 au 13/04</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td> 
<!-- TP -->

</td>
<td class="remarque"> 
<!-- REMARQUE -->

</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->



<!-- ========================================================= -->
<tr class="seance" >
<td>du 15/04 au 29/04</td>
<td>
<!-- COURS -->

</td>
<td>
<!-- TD -->

</td>
<td>
<!-- TP -->

</td>
<td class="remarque"> 
<!-- REMARQUE -->

</td>
</tr>
<!-- ========================================================= -->





<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="libre" >
<td>du 22/04 au 04/05</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td> 
<!-- TP -->

</td>
<td class="remarque"> 
<!-- REMARQUE -->
interruption pédagogique de printemps
</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->





<!-- ========================================================= -->
<!-- SEANCE  -->
<tr class="seance" >
<td>du 06/05 au 11/05</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td> 
<!-- TP -->

</td>
<td class="remarque"> 
<!-- REMARQUE -->
<ul>
<li>mercredi 8 mai férié</li>
<li>jeudi 9 mai férié</li>
</ul>
</td>
</tr>
	<!-- FIN SEANCE  -->
<!-- ========================================================= -->


<!-- ========================================================= -->
<!-- SEANCE  -->
<tr class="seance" >
<td>du 13/05 au 18/05</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td> 
<!-- TP -->

</td>
<td class="remarque">

<!-- REMARQUE -->
</td>
</tr>
<!-- FIN SEANCE  -->
<!-- ========================================================= -->


</table>



</div>



<!-- signature -->
<div class="signature">
   <!-- VOTRE NOM ICI --> <br/>
   dernière modification : 
<?php echo date(" d/m/Y à H:i:s", getlastmod()); ?>
</div>
