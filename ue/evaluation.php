<!-- exemple de description des modalités d'évaluation d'une UE -->
<!-- la classe NOTE attribue une couleur -->
<!-- la classe FORMULE pour les tags  -->

<p>
L'évaluation s'effectue suivant une procédure de contrôle continu. 

Trois notes seront attribuées à chaque étudiant durant le semestre :
</p>

<ul>
<li> <span class="NOTE">TP</span> : une note sur 20 de Travaux Pratiques
 attribuée par l'enseignant de Travaux
Pratiques.  </li> 
<li> <span class="NOTE">DS1</span> : une note sur 20 d'un devoir
surveillé en milieu de semestre.</li> 
<li> <span class="NOTE">DS2</span> : une note sur 20 d'un devoir
surveillé en fin de semestre.</li> 
</ul>




<p>
La note finale sur 20 (<span class="NOTE">N</span>) est calculée comme
une moyenne pondérée de ces notes : 
</p>

<p class="FORMULE">
 N = (TP + sup(DS1 + 2*DS2, 3*DS2)) /4
</p>



<p>La session de rattrapage remplace la partie <i>sup(DS1 + 2*DS2, 3*DS2)</i>, la
note <i>TP</i> est conservée.
</p>
 

<p>
L'unité acquise apporte 5 ECTS.
</p>

<!-- signature -->
<div class="signature">
   <!-- VOTRE NOM ICI --> <br/>
   dernière modification : 
<?php echo date(" d/m/Y à H:i:s", getlastmod()); ?>
</div>
