<!-- onglet "Documents" -->

<h3> Ressources </h3>

<p>Les supports de formation d'ODI sont accessibles depuis la page
<a href="https://gitlab.univ-lille.fr/ls1-odi/portail/-/blob/master/Readme.md">gitlab.univ-lille.fr/ls1-odi/portail/.../Readme.md</a>
</p>

<!-- signature -->
<div class="signature">
   <!-- VOTRE NOM ICI --> <br/>
   dernière modification : 
<?php echo date(" d/m/Y à H:i:s", getlastmod()); ?>
</div>
